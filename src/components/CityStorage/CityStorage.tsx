import { ICityStorage } from "../../types";
import { StorageItem } from "../StorageItem/StorageItem";
import "./CityStorage.scss";

interface CityStorageProps {
  storage: ICityStorage[] | [];
  onBuyGood: (goodId: number, goodValue: number, currentPrice: number) => void;
}

export const CityStorage = ({ storage, onBuyGood }: CityStorageProps) => {

  return (
    <div>
      <h2 className="title">Ярморка</h2>

      <div className="panel">
        <div className="city-goods">
          {storage.map((good) => {
            return (
              <StorageItem key={good.id} good={good} onBuyGood={onBuyGood} />
            );
          })}
        </div>
      </div>
    </div>
  );
};
