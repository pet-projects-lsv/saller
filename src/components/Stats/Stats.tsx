import { settings } from "../../config/settings";
import "./Stats.scss";

interface StatsProps {
  money: number;
  days: number;
}

export const Stats = ({ days, money }: StatsProps) => {
  return (
    <div>
      <h2 className="title">Статистика</h2>

      <div className="panel stats-panel">
        <div className="money">
          {money} / {settings.goalMoney}
        </div>
        <div className="days">
          Дни: {days} / {settings.goalDays}
        </div>
      </div>
    </div>
  );
};
