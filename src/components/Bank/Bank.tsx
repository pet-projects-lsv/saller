import { useState } from "react";
import { IDeposits } from "../../types";
import "./Bank.scss";

interface IBankProps {
  money: number;
  deposits: IDeposits[];
  onOpenDeposit: (amount: number) => void;
}

export const Bank = ({ deposits, onOpenDeposit, money }: IBankProps) => {
  const [amount, setAmount] = useState("");

  return (
    <div>
      <h2 className="title">Банк</h2>

      <div className="panel deposit">
        <div className="sell-panel">
          <div className="sell-panel-content">
            <div className="sell-panel-header">
              <div>Сумма:</div>
              <div className="controls">
                <input
                  className="input"
                  type="text"
                  maxLength={4}
                  value={amount}
                  onChange={(e) => setAmount(e.currentTarget.value)}
                />
                <button
                  className="button"
                  onClick={() => {
                    onOpenDeposit(Number(amount));
                    setAmount("");
                  }}
                  disabled={(amount && money > Number(amount)) ? false : true}
                >
                  Открыть
                </button>
              </div>
            </div>
          </div>
        </div>

        {deposits.map((deposit) => {
          return (
            <div className="good-item-wrapper">
              <div className="good-item-descripten">
                <div className="good good-money" />
              </div>

              <div className="good-item-transport-info">
                <div>
                  <div className="header">Сумма: {deposit.amount}</div>
                  <div className="days">
                    Дней до получения процента: {deposit.days}
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
