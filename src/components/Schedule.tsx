import React from "react";
import { Line } from "react-chartjs-2";

interface IDatasets {
  label: string;
  data: number[];
  fill: boolean;
  backgroundColor: string;
  borderColor: string;
}

interface ISchedule {
  labels: string[];
  datasets: IDatasets[];
}

interface IScheduleProps {
  data: ISchedule;
}

export const Schedule = ({ data }: IScheduleProps) => {
  const options = {
    legend: {
      display: false,
    },
    maintainAspectRatio: false,
    tooltips: {
      mode: "index",
      intersect: false,
      caretSize: 3,
      backgroundColor: "#8d6048",
      bodyFontColor: "#d6ba7a",
      borderColor: "#8d6048",
      borderWidth: 1,
      displayColors: false,

      callbacks: {
        title() {
          return "";
        },
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            stepSize: 1,
            fontColor: '#8d6048',
            beginAtZero: false,
            fontSize: 10
          },
          gridLines: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            display: false,
          },
        },
      ],
    },
  };

  return <Line data={data} options={options} />;
};
