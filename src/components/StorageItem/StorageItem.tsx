import { useState } from "react";
import { ICityStorage } from "../../types";
import { Good } from "../Good/Good";
import { Schedule } from "../Schedule";
import "./StorageItem.scss";

interface IStorageItemProps {
  good: ICityStorage;
  onBuyGood: (goodId: number, goodValue: number, currentPrice: number) => void;
}

export const StorageItem = ({ good, onBuyGood }: IStorageItemProps) => {

  const [goodValue, setGoodValue] = useState<string | number>('');

  const getGoodData = (priceStats: number[]) => {
    return {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8"],
      datasets: [
        {
          label: "Цена за штуку",
          data: priceStats,
          fill: false,
          backgroundColor: "#a68156",
          borderColor: "#8d604844",
        },
      ],
    };
  };

  return (
    <div key={good.id} className="good-item-wrapper">
      <div className="good-info">
        <Good id={good.id} />
        <input
          className="input-good"
          type="text"
          name={"count" + new Date()}
          autoComplete={"new-password"}
          maxLength={3}
          value={String(goodValue)}
          onChange={(e) => {
            setGoodValue(parseInt(e.currentTarget.value, 10) || "")
          }}
        />
        <button
          className="button"
          disabled={!goodValue ? true : false}
          onClick={() => {
            onBuyGood(
              good.id,
              Number(goodValue),
              good.priceStats[good.priceStats.length - 1]
            );
            setGoodValue('0');
          }}
        >
          Купить
        </button>
        <p className="price-description">
          {good.priceStats[good.priceStats.length - 1]} за шт.
        </p>
      </div>
      <div className="good-stats">
        <Schedule data={getGoodData(good.priceStats)} />
      </div>
    </div>
  );
};
