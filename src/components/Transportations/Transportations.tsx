import { getCityNameById } from "../../helpers/getCityNameById";
import { getGoodTitleById } from "../../helpers/getGoodTitleById";
import { ITransportationsGood } from "../../types";
import { Good } from "../Good/Good";
import "./Transportations.scss";

interface ITransportationsProps {
  orders: ITransportationsGood[];
  onAcceptOrder: (order: ITransportationsGood) => void
}

export const Transportations = ({ orders, onAcceptOrder }: ITransportationsProps) => {
  const createPathTransportations = (from: number, to: number) => {
    return `${getCityNameById(from)} - ${getCityNameById(to)}`;
  };

  return (
    <div className="transportations">
      <h2 className="title">Активные перевозки</h2>

      <div className="panel">
        {orders.map((order, index) => {
          return (
            <div key={order.goodId + index} className="good-item-wrapper">
              <div className="god-item-description">
                <Good id={order.goodId} />
              </div>
              <div className="good-transportations-info">
                <div>
                  <div className="header">
                    {getGoodTitleById(order.goodId)}, {order.qty}
                  </div>
                  <div className="path">
                    {createPathTransportations(
                      order.fromCityid,
                      order.targetCityid
                    )}
                  </div>
                </div>
                <div>
                  <div className="days">Дни: {order.days}</div>
                  <button
                    className="button"
                    disabled={order.days ? true : false}
                    onClick={() => {
                      onAcceptOrder(order)
                    }}
                  >
                    Получить
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
