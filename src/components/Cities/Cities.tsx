import "./Cities.scss";
import cn from "classnames";
import { cities } from "../../config/cities";
import { Link } from "react-router-dom";

interface CitiesProps {
  currentCity: number;
  onChangeCity: (cityId: number) => void;
}

export const Cities = ({ currentCity, onChangeCity }: CitiesProps) => {
  return (
    <div className="cities-list">
      {cities.map((city) => {
        return (
          <Link
            to={String(city.title).toLowerCase()}
            key={city.id}
            onClick={() => onChangeCity(city.id)}
            className={cn("city", {
              active: city.id === currentCity,
            })}
          >
            {city.title}
          </Link>
        );
      })}
    </div>
  );
};
