import { useState } from "react";
import { IPlayerStorage } from "../../types";
import { Good } from "../Good/Good";
import { goods } from "../../config/goods";
import "./Storage.scss";
import { cities } from "../../config/cities";

export interface IStorageProps {
  currentCity: number;
  storage?: IPlayerStorage[] | [];
  selectedGood: number | null;
  onSelectGood: (id: number) => void;
  onSell: (id: number, qty: number, price: number) => void;
  onTransport: (targetCityId: number) => void;
  selectedProductPrice: number;
}

export const Storage = ({
  currentCity,
  storage,
  selectedGood,
  onSelectGood,
  onSell,
  onTransport,
  selectedProductPrice,
}: IStorageProps) => {
  const [qty, setQty] = useState<string | number>("");
  const [targetCityId, setTargetCityId] = useState<number>(2);

  const findGoodById = (goodId: number): string => {
    const good = goods?.find((good) => {
      return good.id === goodId;
    })?.title;

    return good ?? "";
  };

  const getTotalPrice = () => {
    const count: number = qty as number;
    return parseInt(String(selectedProductPrice * count * 0.9), 10);
  };

  return (
    <div>
      <h2 className="title">Мой склад</h2>

      <div className="panel">
        <ul className="goods">
          <>
            {storage &&
              Array(8)
                .fill("")
                .map((i, index) => {
                  const item = storage[index];
                  return (
                    <Good
                      key={index}
                      id={item?.id}
                      qty={item?.qty}
                      selectedGood={selectedGood}
                      onSelectedGood={() => onSelectGood(item?.id)}
                    />
                  );
                })}
          </>
        </ul>
        {selectedGood ? (
          <>
            <div className="sell-panel">
              <div className="sell-panel-content">
                <div className="sell-panel-header">
                  <div>{findGoodById(selectedGood)}</div>
                  <div className="controls">
                    <input
                      className="input"
                      type="text"
                      value={qty}
                      onChange={(e) =>
                        setQty(parseInt(e.currentTarget.value, 10) || "")
                      }
                    />
                    <span className="unit">шт.</span>
                    <button
                      className="button"
                      onClick={() =>
                        onSell(selectedGood, Number(qty), getTotalPrice())
                      }
                      disabled={!qty || !selectedProductPrice}
                    >
                      Продать
                    </button>
                  </div>
                </div>
                {qty && selectedProductPrice ? (
                  <div className="sell-panel-info">
                    По цене {selectedProductPrice} x {qty} шт, налог: 10%. Итого{" "}
                    {getTotalPrice()}
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
            <div className="order-panel">
              <div className="">
                <select
                  name=""
                  id=""
                  className="select-сity"
                  value={targetCityId}
                  onChange={(e) =>
                    setTargetCityId(parseInt(e.currentTarget.value, 10))
                  }
                >
                  {cities.map((city) => {
                    return (
                      <option
                        key={city.id}
                        value={city.id}
                        disabled={city.id === currentCity}
                      >
                        {city.title}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="controls">
                <button
                  className="button"
                  onClick={() => {
                    onTransport(targetCityId);
                  }}
                >
                  Поехали
                </button>
              </div>
            </div>
          </>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};
