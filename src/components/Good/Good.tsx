import "./Good.scss";

interface IGooProps {
  id?: number;
  selectedGood?: number | null;
  onSelectedGood?: () => void;
  qty?: number;
}

export const Good = ({ id, qty, selectedGood = null, onSelectedGood }: IGooProps) => {
  const generateClassNameGood = (
    itemId: number,
    selectedGood: number | null
  ) => {
    return `good good-${itemId} ${selectedGood === itemId ? "selected" : ""}`;
  };

  return (
    <div onClick={() => onSelectedGood?.()} className={id ? generateClassNameGood(id, selectedGood) : 'good'}>
      {qty && <span className="good-description">{qty}</span>}
    </div>
  );
};
