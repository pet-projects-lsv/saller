export interface ICityStorageItem {
  cityId: number;
  storage: ICityStorage[];
}

export interface IPlayerStorageItem {
  cityId: number;
  storage: IPlayerStorage[];
}

export interface IStorageitem {
  cityId: number;
  storage: IPlayerStorage[] | ICityStorage[];
}

export interface ICityStorage {
  id: number;
  priceStats: number[];
  maxStep: number;
  minPrice: number;
  maxPrice: number;
  qty?: number
}

export interface IPlayerStorage {
  id: number;
  qty: number;
}

export interface IGoodEntity {
  id: number;
  title: string;
}

export interface ITransportationsGood {
  id: number;
  fromCityid: number;
  targetCityid: number;
  goodId: number;
  qty: number;
  days: number;
}

export interface IDeposits {
  id: number;
  amount: number;
  days: number;
}
