import { nanoid } from "nanoid";
import { useEffect, useState } from "react";
import { efaultCityStorages } from "../config/defaultCityStorages";
import { defaultStorages } from "../config/defaultStorages";
import { getStatuses, settings } from "../config/settings";
import { getRandomInt } from "../helpers/getRandomInt";
import {
  ICityStorage,
  ICityStorageItem,
  IPlayerStorage,
  IPlayerStorageItem,
  IStorageitem,
  ITransportationsGood,
} from "../types";

export const useAppLogic = () => {
  const [gameStatus, setGameStatus] = useState(getStatuses.new);
  const [currentCity, setCurrentCity] = useState(1);
  const [money, setMoney] = useState(settings.startMoney);
  const [days, setDays] = useState(1);
  const [selectedGood, setSelectedGood] = useState<number | null>(1);
  const [orderId, setOrderId] = useState<number>(1);
  const [deposits, setDeposits] = useState([
    {
      id: 1,
      amount: 100,
      days: 10,
    },
    {
      id: 1,
      amount: 200,
      days: 20,
    },
  ]);
  const [transportOrders, setTransportOrders] = useState<
    ITransportationsGood[]
  >([]);

  const [playerStorages, setPlayerStorages] =
    useState<IPlayerStorageItem[]>(defaultStorages);

  const [cityStorages, setCityStorages] =
    useState<ICityStorageItem[]>(efaultCityStorages);

  const getCurrentStorage = (
    storages: IStorageitem[]
  ): ICityStorage[] | IPlayerStorage[] => {
    const store = storages.find((storage) => {
      return storage.cityId === currentCity;
    });

    if (store) {
      return store.storage;
    } else {
      return [];
    }
  };

  const updateCityStorages = () => {
    const newCityStorages = cityStorages;

    for (let cityIndex = 0; cityIndex < newCityStorages.length; cityIndex++) {
      const storage = newCityStorages[cityIndex].storage;

      for (let goodIndex = 0; goodIndex < storage.length; goodIndex++) {
        const goodData = storage[goodIndex];
        const pricChangeSing = getRandomInt(2) ? 1 : -1;
        const priceChangeValue =
          getRandomInt(goodData.maxStep + 1) * pricChangeSing;

        let newPrice =
          (goodData.priceStats.slice(-1).pop() ?? 0) + priceChangeValue;

        if (newPrice > goodData.maxPrice) {
          newPrice = goodData.maxPrice;
        }

        if (newPrice < goodData.minPrice) {
          newPrice = goodData.minPrice;
        }

        for (let i = 0; i < goodData.priceStats.length; i++) {
          goodData.priceStats[i] = goodData.priceStats[i + 1];
        }

        goodData.priceStats[goodData.priceStats.length - 1] = newPrice;

        newCityStorages[cityIndex].storage[goodIndex] = goodData;
      }
    }
    setCityStorages(newCityStorages);
  };

  const updateTransportOrders = () => {
    setTransportOrders((old) => {
      const newOld = [...old];

      newOld.forEach((order) => {
        if (order.days >= 1) {
          order.days -= 1;
        }
      });

      return newOld;
    });
  };

  const updateDeposits = () => {
    setDeposits((old) => {
      const newOld = [...old];

      newOld.forEach((deposit, index) => {
        if (deposit.days >= 1) {
          deposit.days -= 1;
        }
        if (deposit.days === 0) {
          newOld.splice(index, 1);

          setMoney((old) => old + deposit.amount * 1.1);
        }
      });

      return newOld;
    });
  };

  const checkGameStatus = (days: number) => {
    if (days >= settings.goalDays && money < settings.goalMoney) {
      setGameStatus(getStatuses.fail);
    }

    if (money > settings.goalMoney) {
      setGameStatus(getStatuses.win);
    }
  };

  const lifeProccess = () => {
    setTimeout(() => {
      updateCityStorages();
      updateDeposits();
      updateTransportOrders();
      checkGameStatus(days + 1)

      setDays((old) => old + 1);
    }, 5000);
  };

  useEffect(() => {
    if (gameStatus === getStatuses.new) {
        lifeProccess();
    }
  }, [days]);

  const sellGoods = (goodId: number, qty: number, totalPrice: number) => {
    const storagesNew = [...playerStorages];
    let moneyNew = money;

    const index = storagesNew.findIndex((storage) => {
      return storage.cityId === currentCity;
    });

    if (index > -1) {
      const goodIndex = storagesNew[index].storage.findIndex(
        (good: { id: number; qty: number }) => {
          return good.id === goodId;
        }
      );

      if (goodIndex > -1) {
        const currentCityStorage = getCurrentStorage(cityStorages);

        const cityGoodIndex = currentCityStorage.findIndex((good) => {
          return good.id === goodId;
        });

        if (cityGoodIndex > -1) {
          if (storagesNew[index].storage[goodIndex].qty >= qty) {
            playerStorages[index].storage[goodIndex].qty -= qty;
            moneyNew += totalPrice;
            if (!playerStorages[index].storage[goodIndex].qty) {
              removeGood(playerStorages[index].storage[goodIndex].id);
            }
            setMoney(moneyNew);
          }
        }
      }
    }

    setPlayerStorages(storagesNew);
  };

  const buyGoods = (goodId: number, qty: number, price: number) => {
    const totalPrice = qty * price;

    if (money >= totalPrice && qty > 0) {
      // изменить значение товаров и
      const storagesNew = playerStorages;

      const index = storagesNew.findIndex((storage) => {
        return storage.cityId === currentCity;
      });

      if (index > -1) {
        const goodIndex = storagesNew[index].storage.findIndex((good) => {
          return good.id === goodId;
        });

        if (goodIndex > -1) {
          playerStorages[index].storage[goodIndex].qty += qty;
        } else {
          playerStorages[index].storage.push({
            id: goodId,
            qty,
          });
        }
      }

      setPlayerStorages(storagesNew);
      setMoney(money - totalPrice);
    }
  };

  const createTransportOrder = (targetCityid: number) => {
    const newOrders = [...transportOrders];

    const storage = getCurrentStorage(playerStorages);

    const goodIndex = storage.findIndex((good) => good.id === selectedGood);

    if (goodIndex > -1) {
      newOrders.push({
        id: orderId,
        fromCityid: currentCity,
        targetCityid,
        goodId: selectedGood as number,
        qty: storage[goodIndex].qty as number,
        days: 5,
      });

      setOrderId(orderId + 1);
      setTransportOrders(newOrders);
      removeGood(selectedGood as number);
    }
  };

  const removeGood = (goodId: number): void => {
    const storagesNew = playerStorages;

    const index = storagesNew.findIndex((storage) => {
      return storage.cityId === currentCity;
    });

    if (index > -1) {
      const goodIndex = storagesNew[index].storage.findIndex((good) => {
        return good.id === goodId;
      });

      if (storagesNew[index].storage[goodId].qty >= 1) {
        storagesNew[index].storage.splice(goodIndex, 1);
      }
    }
  };

  const acceptOrder = (order: ITransportationsGood) => {
    setTransportOrders((orders) => {
      const newOrders = [...orders];

      const index = newOrders.findIndex((o) => {
        return o.goodId === order.goodId;
      });

      if (index > -1) {
        newOrders.splice(index, 1);
      }

      return newOrders;
    });

    const storagesNew = playerStorages;

    const index = storagesNew.findIndex((storage) => {
      return storage.cityId === order.targetCityid;
    });

    if (index > -1) {
      const goodIndex = storagesNew[index].storage.findIndex((good) => {
        return good.id === order.goodId;
      });

      if (goodIndex > -1) {
        storagesNew[index].storage[goodIndex].qty += order.qty;
      } else {
        playerStorages[index].storage.push({
          id: order.goodId,
          qty: order.qty,
        });
      }
    }

    setPlayerStorages(storagesNew);
  };

  const getSelectedproductPrice = () => {
    const cityStorage = getCurrentStorage(cityStorages) as ICityStorage[];

    const product = cityStorage.find((good) => {
      return good.id === selectedGood;
    });

    if (product && product.priceStats) {
      return product.priceStats[product.priceStats.length - 1];
    } else {
      return 0;
    }
  };

  const openDeposit = (amount: number) => {
    if (amount > 0 && money >= amount) {
      setDeposits((old) => {
        const newOld = [...old];

        newOld.push({
          id: Number(nanoid(4)),
          days: 30,
          amount,
        });

        setMoney((old) => old - amount);

        return newOld;
      });
    }
  };

  return {
    currentCity,
    setCurrentCity,
    getCurrentStorage,
    cityStorages,
    playerStorages,
    selectedGood,
    getSelectedproductPrice,
    setSelectedGood,
    sellGoods,
    createTransportOrder,
    transportOrders,
    acceptOrder,
    days,
    money,
    deposits,
    buyGoods,
    openDeposit,
  };
};
