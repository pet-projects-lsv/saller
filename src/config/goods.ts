import { IGoodEntity } from "../types";

export const goods: IGoodEntity[] = [
  {
    id: 1,
    title: "Пиво",
  },
  {
    id: 2,
    title: "Молоко",
  },
  {
    id: 3,
    title: "Пшеница",
  },
  {
    id: 4,
    title: "Грибы",
  },
  {
    id: 5,
    title: "Клевер",
  },
  {
    id: 6,
    title: "Репа",
  },
  {
    id: 7,
    title: "Виноград",
  },
  {
    id: 8,
    title: "Орехи",
  },
  {
    id: 9,
    title: "Вилы",
  },
  {
    id: 10,
    title: "Доски",
  },
  {
    id: 11,
    title: "Коса",
  },
  {
    id: 12,
    title: "Лопата",
  },
  {
    id: 13,
    title: "Топор",
  },
  {
    id: 14,
    title: "Кирка",
  },
  {
    id: 0,
    title: "Деньги",
  },
];
