import { IPlayerStorageItem } from "../types";

export const defaultStorages: IPlayerStorageItem[] = [
  {
    cityId: 1,
    storage: [
      {
        id: 1,
        qty: 11,
      },
      {
        id: 2,
        qty: 10,
      },
      {
        id: 3,
        qty: 10,
      },
      //   {
      //     id: 4,
      //     qty: 10,
      //   },
      //   {
      //     id: 5,
      //     qty: 10,
      //   },
      {
        id: 12,
        qty: 10,
      },
      {
        id: 7,
        qty: 10,
      },
      {
        id: 13,
        qty: 10,
      },
    ],
  },
  {
    cityId: 2,
    storage: [
      {
        id: 9,
        qty: 5,
      },
      {
        id: 10,
        qty: 10,
      },
      {
        id: 11,
        qty: 10,
      },
      {
        id: 12,
        qty: 10,
      },
      {
        id: 13,
        qty: 10,
      },
      {
        id: 14,
        qty: 10,
      },
    ],
  },
];
