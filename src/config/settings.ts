export const settings = {
    goalMoney: 2000,
    goalDays: 100,
    startMoney: 1000
}

export const getStatuses = {
    new: 1,
    fail: 2,
    win: 3
}