export const cities = [
  { id: 1, title: "Новгород" },
  { id: 2, title: "Киев" },
  { id: 3, title: "Владимир" },
  { id: 4, title: "Суздаль" },
  { id: 5, title: "Ярославль" },
  { id: 6, title: "Чернигов" },
  { id: 7, title: "Москва" },
];
