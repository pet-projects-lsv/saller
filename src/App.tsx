import "./App.scss";
import { Cities } from "./components/Cities/Cities";
import { CityStorage } from "./components/CityStorage/CityStorage";
import { Stats } from "./components/Stats/Stats";
import { Storage } from "./components/Storage/Storage";
import { Transportations } from "./components/Transportations/Transportations";
import { ICityStorage, IPlayerStorage } from "./types";
import { Bank } from "./components/Bank/Bank";
import { useAppLogic } from "./hooks/useAppLogic";

function App() {
  const {
    currentCity,
    setCurrentCity,
    getCurrentStorage,
    cityStorages,
    playerStorages,
    selectedGood,
    getSelectedproductPrice,
    setSelectedGood,
    sellGoods,
    createTransportOrder,
    transportOrders,
    acceptOrder,
    days,
    money,
    deposits,
    buyGoods,
    openDeposit
  } = useAppLogic();
  return (
    <div className="app">
      <div className="app-content">
        <h1 className="app-name">Купечество</h1>

        <Cities currentCity={currentCity} onChangeCity={setCurrentCity} />

        <div className="content">
          <div className="column">
            <div className="storage">
              <Storage
                currentCity={currentCity}
                storage={
                  getCurrentStorage(playerStorages) as IPlayerStorage[] | []
                }
                selectedGood={selectedGood}
                onSelectGood={(goodId: number) => {
                  setSelectedGood(goodId);
                }}
                onSell={(id: number, qty: number, price: number) => {
                  sellGoods(id, qty, price);
                }}
                onTransport={(targetCityId) => {
                  createTransportOrder(targetCityId);
                }}
                selectedProductPrice={getSelectedproductPrice()}
              />
            </div>
            <div className="transportations">
              <Transportations
                orders={transportOrders}
                onAcceptOrder={acceptOrder}
              />
            </div>
            <div className="stats">
              <Stats money={money} days={days} />
            </div>
            <div className="deposits">
              <Bank deposits={deposits} onOpenDeposit={openDeposit} money={money} />
            </div>
          </div>
          <div className="column">
            <CityStorage
              storage={getCurrentStorage(cityStorages) as ICityStorage[] | []}
              onBuyGood={(goodId, goodValue, price) =>
                buyGoods(goodId, goodValue, price)
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
