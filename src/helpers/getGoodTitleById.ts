import { goods } from "../config/goods";

export const getGoodTitleById = (id: number) => {
   const good = goods.find((i) => {
    return i.id === id;
   })

   return good?.title
};
