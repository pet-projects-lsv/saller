import { cities } from "../config/cities";

export const getCityNameById = (id: number) => {
   const good = cities.find((i) => {
    return i.id === id;
   })

   return good?.title
};
